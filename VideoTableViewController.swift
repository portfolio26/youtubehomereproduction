//
//  VideoTableViewController.swift
//  YouTubeHomeScreen
//
//  Created by Francesco Valente on 05/12/2019.
//  Copyright © 2019 Francesco Valente. All rights reserved.
//

import UIKit
import AVKit

class VideoTableViewController: UITableViewController {
    
    //MARK: Properties
    var videos = [Video]();
    @IBOutlet weak var airButton: UIBarButtonItem!
    @IBOutlet weak var videoCreationButton: UIBarButtonItem!
    
    //MARK: Actions
    @IBAction func displayActionSheet(_ sender: Any) {
        let optionMenu = UIAlertController(title: nil, message: "Collega a un dispositivo:", preferredStyle: .actionSheet)
        
        let action1 = UIAlertAction(title: "AirPlay & Bluetooth devices", style: .default)
        let action2 = UIAlertAction(title: "Ulteriori Informazioni", style: .default)
        let cancelAction = UIAlertAction(title: "Annulla", style: .cancel)
        
        
        
        optionMenu.addAction(action1)
        optionMenu.addAction(action2)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
        
        
    }
    
    @IBAction func displayVideoCreation(_ sender: Any) {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load the sample data.
        loadSampleVideos()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return videos.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "VideoTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? VideoTableViewCell else{
            fatalError("The dequeued cell is not an instance of VideoTableViewCell.")
        }
        
        let video = videos[indexPath.row]

        // Configure the cell...
        cell.videoPreview.image = video.videoPreview
        cell.videoName.text = video.videoName
        cell.profileImage.image = video.profileImage
        cell.profileName.text = video.profileName
        cell.visualisations.text = video.visualisation
        cell.uploadTime.text = video.uploadTime
        

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let videoUrl = videos[indexPath.row].videoUrl
        let video = AVPlayer(url: videoUrl)
        let videoPlayer = AVPlayerViewController()
        videoPlayer.player = video
        
        present(videoPlayer, animated: true, completion: {
            video.play()
        })
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: Private Methods
     
    private func loadSampleVideos() {
        let videoPreview1 = UIImage(named : "fire")!
        let videoPreview2 = UIImage(named : "barbecue")
        let videoPreview3 = UIImage(named : "rollerCoaster")
        
        let profileImage1 = UIImage(named : "profile1")
        let profileImage2 = UIImage(named: "profile2")
        let profileImage3 = UIImage(named : "profile3")
        
        let videoUrlPath1 = Bundle.main.path(forResource: "Burning Charcoal Fire", ofType: "mp4")!
        let videoUrl1 = URL(fileURLWithPath: videoUrlPath1)
        let videoUrlPath2 = Bundle.main.path(forResource: "Pexels Videos 3972", ofType: "mp4")!
        let videoUrl2 = URL(fileURLWithPath: videoUrlPath2)
        let videoUrlPath3 = Bundle.main.path(forResource: "Roller Coasters", ofType: "mp4")!
        let videoUrl3 = URL(fileURLWithPath: videoUrlPath3)
        
        
        guard let video1 = Video(videoPreview : videoPreview1, videoName : "Around the fire", profileImage : profileImage1!, profileName : "Francesco", visualisation : "500k visualizations", uploadTime : "2 weeks ago", videoUrl: videoUrl1)
            else{
            fatalError("Unable to instantiate video1")
        }
        
        guard let video2 = Video(videoPreview : videoPreview2!, videoName : "Sunday barbecue", profileImage : profileImage2!, profileName : "Annabelle", visualisation : "320 visualizations", uploadTime : "3 years ago", videoUrl: videoUrl2)
            else{
            fatalError("Unable to instantiate video1")
        }
        
        guard let video3 = Video(videoPreview : videoPreview3!, videoName : "Having fun with the Roller Coaster", profileImage : profileImage3!, profileName : "Christine", visualisation : "3Mln visualizations", uploadTime : "1 week ago", videoUrl: videoUrl3)
            else{
            fatalError("Unable to instantiate video1")
        }
        
        videos += [video1, video2, video3]

        
    }

}
