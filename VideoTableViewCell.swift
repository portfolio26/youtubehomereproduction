//
//  VideoTableViewCell.swift
//  YouTubeHomeScreen
//
//  Created by Francesco Valente on 04/12/2019.
//  Copyright © 2019 Francesco Valente. All rights reserved.
//

import UIKit

class VideoTableViewCell: UITableViewCell {
    //MARK: Properties
    @IBOutlet weak var videoPreview: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var videoName: UILabel!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var visualisations: UILabel!
    @IBOutlet weak var uploadTime: UILabel!

    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        profileImage.layer.borderWidth = 0
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.white.cgColor
        profileImage.layer.cornerRadius = profileImage
            .frame.height/2
        profileImage.clipsToBounds = true
    }

}
