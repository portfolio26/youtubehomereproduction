//
//  Video.swift
//  YouTubeHomeScreen
//
//  Created by Francesco Valente on 05/12/2019.
//  Copyright © 2019 Francesco Valente. All rights reserved.
//

import Foundation
import UIKit

class Video{
    //MARK: Properties
    var videoPreview : UIImage
    var videoName : String
    var profileImage : UIImage
    var profileName : String
    var visualisation : String
    var uploadTime : String
    var videoUrl : URL
    
    //Initialization
    init?(videoPreview : UIImage, videoName : String, profileImage : UIImage, profileName : String, visualisation : String, uploadTime : String, videoUrl : URL){
        
        if(videoName.isEmpty || profileName.isEmpty){
            return nil
        }
        
        self.videoPreview = videoPreview
        self.videoName = videoName
        self.profileImage = profileImage
        self.profileName = profileName
        self.visualisation = visualisation
        self.uploadTime = uploadTime
        self.videoUrl = videoUrl
    }
    
}
